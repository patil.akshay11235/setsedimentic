# setSedimentIc

This utility sets the sediment profile using a linear profile with C=1 at the bottom of the wall and C=0 at the top of the wall.